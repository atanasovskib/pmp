package edu.fcse.procesmodelparser.parsers.generators;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import edu.fcse.procesmodelparser.models.ImportDescription;
import edu.fcse.procesmodelparser.models.services.ServiceModel;
import edu.fcse.procesmodelparser.parsers.FileLibrary;
import edu.fcse.procesmodelparser.parsers.GenericParser;
import edu.fcse.procesmodelparser.parsers.ModelLibrary;

import javax.lang.model.element.Modifier;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.function.Function.identity;

public class ServiceGenerator extends Generator {

    private final ObjectMapper objectMapper;
    private final String servicesPackageRelative;

    private final Path servicesPath;

    public ServiceGenerator(Path rootPath,
                            ObjectMapper objectMapper,
                            String servicesPackageRelative) {
        super(rootPath);
        this.objectMapper = objectMapper;
        this.servicesPackageRelative = servicesPackageRelative;
        this.servicesPath = rootPath.resolve(servicesPackageRelative);
    }

    @Override
    public void parse(String rootPackage,
                      ModelLibrary modelLibrary,
                      FileLibrary fileLibrary,
                      GenericParser genericParser,
                      Path modelToParse) {
        ServiceModel model = this.readModel(ServiceModel.class, this.objectMapper, modelToParse);

        Map<String, ImportDescription> importsById = Arrays.stream(model.imports)
                .collect(Collectors.toMap(x -> x.id, identity()));

        if (importsById.size() != model.imports.length) {
            throw new IllegalStateException("Imports in " + modelToParse + " contain duplicates by id");
        }

        Arrays.stream(model.imports)
                .filter(ImportDescription::isLocal)
                .filter(imp -> !modelLibrary.hasAnyTypeOfModel(imp.id))
                .forEach(imp -> {
                    String subFolder = imp.location.substring(1);
                    Path subModelPath = this.rootPath
                            .resolve(subFolder)
                            .resolve(imp.id + ".json");
                    genericParser.parse(rootPackage, modelLibrary, fileLibrary, subModelPath);
                });

        TypeName returnType = fileLibrary.getTypeName(rootPackage, model.output, importsById);
        MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder(model.name)
                .addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT)
                .returns(returnType);

        for (InputDescription inputParam : model.input) {
            TypeName className = fileLibrary.getTypeName(rootPackage, inputParam.type, importsById);
            methodBuilder.addParameter(className, inputParam.name);
        }

        TypeSpec serviceSpec = TypeSpec.interfaceBuilder(model.name)
                .addModifiers(Modifier.PUBLIC)
                .addMethod(methodBuilder.build())
                .build();

        String servicesPackage = fileLibrary.packageid(rootPackage, "." + servicesPackageRelative);
        JavaFile javaFile = JavaFile.builder(servicesPackage, serviceSpec).build();
        fileLibrary.addFile(model.name, javaFile);
        modelLibrary.addStepModel(model);
    }

    @Override
    public Path getPath() {
        return this.servicesPath;
    }
}
