package edu.fcse.procesmodelparser.parsers.generators;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.fcse.procesmodelparser.parsers.FileLibrary;
import edu.fcse.procesmodelparser.parsers.GenericParser;
import edu.fcse.procesmodelparser.parsers.ModelLibrary;

import java.io.IOException;
import java.nio.file.Path;

public abstract class Generator {

    protected final Path rootPath;

    public Generator(Path rootPath){
        this.rootPath = rootPath;
    }

    public abstract void parse(String rootPackage,
                               ModelLibrary modelLibrary,
                               FileLibrary fileLibrary,
                               GenericParser genericParser,
                               Path modelToParse);

    public abstract Path getPath();

    public <T> T readModel(Class<T> modelClass, ObjectMapper objectMapper, Path modelPath) {
        try {
            return objectMapper.readValue(modelPath.toFile(), modelClass);
        } catch (IOException e) {

            throw new RuntimeException("Could not parse " + modelClass.getName() + " in file " + modelPath, e);
        }
    }


}
