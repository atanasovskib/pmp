package edu.fcse.procesmodelparser.parsers.generators;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.fcse.procesmodelparser.models.ImportDescription;
import edu.fcse.procesmodelparser.models.processes.UserInputModel;
import edu.fcse.procesmodelparser.parsers.FileLibrary;
import edu.fcse.procesmodelparser.parsers.GenericParser;
import edu.fcse.procesmodelparser.parsers.ModelLibrary;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.Map;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

public class UserInputGenerator extends Generator {

    private final ObjectMapper objectMapper;

    private final Path userInputPath;

    public UserInputGenerator(Path rootPath,
                              ObjectMapper objectMapper,
                              String userInputPackageRelative) {
        super(rootPath);
        this.objectMapper = objectMapper;
        this.userInputPath = rootPath.resolve(userInputPackageRelative);
    }

    @Override
    public void parse(String rootPackage,
                      ModelLibrary modelLibrary,
                      FileLibrary fileLibrary,
                      GenericParser genericParser,
                      Path modelToParse) {
        UserInputModel model = this.readModel(UserInputModel.class, this.objectMapper, modelToParse);

        Map<String, ImportDescription> importsById = Arrays.stream(model.imports)
                .collect(toMap(x -> x.id, identity()));

        if (importsById.size() != model.imports.length) {
            throw new IllegalStateException("Imports in " + modelToParse + " contain duplicates by id");
        }

        Arrays.stream(model.imports)
                .filter(ImportDescription::isLocal)
                .filter(imp -> !modelLibrary.hasAnyTypeOfModel(imp.id))
                .forEach(imp -> {
                    String subFolder = imp.location.substring(1);
                    Path subModelPath = this.rootPath
                            .resolve(subFolder)
                            .resolve(imp.id + ".json");
                    genericParser.parse(rootPackage, modelLibrary, fileLibrary, subModelPath);
                });

        modelLibrary.addStepModel(model);
    }

    @Override
    public Path getPath() {
        return this.userInputPath;
    }
}
