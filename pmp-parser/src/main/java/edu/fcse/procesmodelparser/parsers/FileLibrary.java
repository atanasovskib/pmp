package edu.fcse.procesmodelparser.parsers;

import com.squareup.javapoet.ArrayTypeName;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.TypeName;
import edu.fcse.procesmodelparser.models.ImportDescription;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileLibrary {
    private final Map<String, JavaFile> fileMap;

    public FileLibrary(Map<String, JavaFile> fileMap) {
        this.fileMap = new HashMap<>();
        this.fileMap.putAll(fileMap);
    }

    public FileLibrary() {
        this.fileMap = new HashMap<>();
    }

    public void addFile(String classId, JavaFile file) {
        fileMap.put(classId, file);
    }

    public boolean hasFile(String classId) {
        return fileMap.containsKey(classId);
    }

    public JavaFile getFile(String classId) {
        return fileMap.get(classId);
    }


    public void addFiles(List<JavaFile> newFiles) {
        for (JavaFile file : newFiles) {
            String classId = file.packageName + "." + file.typeSpec.name;
            this.fileMap.put(classId, file);
        }
    }

    public String classId(String rootPackage, String classPackage, String className) {
        return packageid(rootPackage, classPackage) + "." + className;
    }

    public String packageid(String rootPackage, String classPackage) {
        return rootPackage + classPackage;
    }

    public TypeName getPrimitive(String typeName) {
        boolean isArray = typeName.endsWith("[]");
        String stemType = isArray ? typeName.substring(0, typeName.length() - 2) : typeName;
        TypeName toReturn = null;
        switch (stemType) {
            case "int":
                toReturn = TypeName.INT;
                break;
            case "long":
                toReturn = TypeName.LONG;
                break;
            case "double":
                toReturn = TypeName.DOUBLE;
                break;
            case "float":
                toReturn = TypeName.FLOAT;
                break;
            case "char":
                toReturn = TypeName.CHAR;
                break;
            case "byte":
                toReturn = TypeName.BYTE;
                break;
            case "boolean":
                toReturn = TypeName.BOOLEAN;
                break;
            case "Object":
                toReturn = TypeName.OBJECT;
                break;
            case "short":
                toReturn = TypeName.SHORT;
                break;
        }

        if (toReturn == null) {
            throw new IllegalArgumentException(typeName + " is not a primitive");
        }

        return isArray ? ArrayTypeName.of(toReturn) : toReturn;
    }

    public TypeName getTypeName(String rootPackage, String typeId, Map<String, ImportDescription> importsById) {
        char firstLetterOfType = typeId.charAt(0);
        boolean isPrimitive = Character.isLowerCase(firstLetterOfType);

        if (isPrimitive) {
            return getPrimitive(typeId);
        }

        boolean isArray = typeId.endsWith("[]");
        String stemType = isArray ? typeId.substring(0, typeId.length() - 2) : typeId;
        ImportDescription importDesc = importsById.get(stemType);
        if (importDesc == null) {
            throw new IllegalStateException("Type " + typeId + " not found in imports map");
        }

        String root = importDesc.isLocal ? rootPackage : "";
        String packageId = this.packageid(root, importDesc.location);
        ClassName stemClassName = ClassName.get(packageId, importDesc.id);
        return isArray ? ArrayTypeName.of(stemClassName) : stemClassName;
    }
}
