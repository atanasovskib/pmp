package edu.fcse.procesmodelparser.parsers;

import edu.fcse.procesmodelparser.models.StepImplementation;
import edu.fcse.procesmodelparser.models.entities.DataObject;

import java.util.HashMap;
import java.util.Map;

public class ModelLibrary {
    private final Map<String, StepImplementation> parsedStepModels;
    private final Map<String, DataObject> parsedDataObjects;

    public ModelLibrary() {
        this.parsedStepModels = new HashMap<>();
        this.parsedDataObjects = new HashMap<>();
    }

    public ModelLibrary(Map<String, StepImplementation> stepModels, Map<String, DataObject> dataObjects) {
        this();
        this.parsedStepModels.putAll(stepModels);
        this.parsedDataObjects.putAll(dataObjects);
    }

    public void addStepModel(StepImplementation model) {
        this.parsedStepModels.put(model.getStepId(), model);
    }

    public void addDataModel(DataObject model) {
        this.parsedDataObjects.put(model.name, model);
    }

    public StepImplementation getStepModel(String stepId) {
        if (!this.parsedStepModels.containsKey(stepId)) {
            throw new RuntimeException("Step " + stepId + " not found in library");
        }

        return this.parsedStepModels.get(stepId);
    }

    public DataObject getDataObject(String dataObject) {
        if (!this.parsedDataObjects.containsKey(dataObject)) {
            throw new RuntimeException("Data object " + dataObject + " not found in library");
        }

        return this.parsedDataObjects.get(dataObject);
    }

    public boolean hasStepModel(String stepId) {
        return this.parsedStepModels.containsKey(stepId);
    }

    public boolean hasDataObject(String dataObject) {
        return this.parsedDataObjects.containsKey(dataObject);
    }

    public boolean hasAnyTypeOfModel(String id) {
        return hasStepModel(id) || hasDataObject(id);
    }
}
