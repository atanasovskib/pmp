package edu.fcse.procesmodelparser.parsers.generators;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.fcse.procesmodelparser.models.StepImplementation;
import edu.fcse.procesmodelparser.models.processes.ParsedProcessModel;
import edu.fcse.procesmodelparser.models.processes.ProcessModel;
import edu.fcse.procesmodelparser.models.processes.StepModel;
import edu.fcse.procesmodelparser.parsers.FileLibrary;
import edu.fcse.procesmodelparser.parsers.GenericParser;
import edu.fcse.procesmodelparser.parsers.ModelLibrary;

import java.nio.file.Path;

public class ProcessGenerator extends Generator {
    private final ObjectMapper objectMapper;
    private final Path processesPath;

    public ProcessGenerator(Path rootPath, ObjectMapper objectMapper, String processesFolder) {
        super(rootPath);
        this.objectMapper = objectMapper;
        this.processesPath = rootPath.resolve(processesFolder);
    }

    @Override
    public void parse(String rootPackage,
                      ModelLibrary modelLibrary,
                      FileLibrary fileLibrary,
                      GenericParser genericParser,
                      Path modelToParse) {
        ProcessModel model = this.readModel(ProcessModel.class, objectMapper, modelToParse);
        for (StepModel step : model.steps) {
            if (modelLibrary.hasStepModel(step.id)) {
                continue;
            }

            String subfolder = step.location.substring(1);
            Path submodelPath = this.rootPath.resolve(subfolder)
                    .resolve(step.id + ".json");
            genericParser.parse(rootPackage, modelLibrary, fileLibrary, submodelPath);
        }

        String startingTaskId = model.startingTask;
        String endingTaskId = model.endingTask;

        StepImplementation startingStep = modelLibrary.getStepModel(startingTaskId);
        StepImplementation endingStep = modelLibrary.getStepModel(endingTaskId);
        ParsedProcessModel parsedProcessModel = new ParsedProcessModel(model, startingStep, endingStep);
        modelLibrary.addStepModel(parsedProcessModel);
    }

    @Override
    public Path getPath() {
        return this.processesPath;
    }
}
