package edu.fcse.procesmodelparser.parsers;

import edu.fcse.procesmodelparser.parsers.generators.Generator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;

public class GenericParser {
    private static final Logger log = LoggerFactory.getLogger(GenericParser.class);

    private final Generator dtoGenerator;
    private final Generator serviceGenerator;
    private final Generator entitiesGenerator;
    private final Generator processesGenerator;
    private final Generator userInputGenerator;

    public GenericParser(Generator dtoGenerator,
                         Generator serviceGenerator,
                         Generator entitiesGenerator,
                         Generator processesGenerator,
                         Generator userInputGenerator) {
        this.dtoGenerator = dtoGenerator;
        this.serviceGenerator = serviceGenerator;
        this.entitiesGenerator = entitiesGenerator;
        this.processesGenerator = processesGenerator;
        this.userInputGenerator = userInputGenerator;
    }

    public void parse(String rootPackage, ModelLibrary modelLibrary, FileLibrary fileLibrary, Path modelToParse) {
        Generator chosenGenerator;
        if (modelToParse.startsWith(this.dtoGenerator.getPath())) {
            chosenGenerator = dtoGenerator;
        } else if (modelToParse.startsWith(this.entitiesGenerator.getPath())) {
            chosenGenerator = entitiesGenerator;
        } else if (modelToParse.startsWith(this.serviceGenerator.getPath())) {
            chosenGenerator = serviceGenerator;
        } else if (modelToParse.startsWith(this.processesGenerator.getPath())) {
            chosenGenerator = processesGenerator;
        } else if (modelToParse.startsWith(this.userInputGenerator.getPath())) {
            chosenGenerator = userInputGenerator;
        } else {
            String error = "Model path [" + modelToParse + "] is not in the correct path for any of the specific generators";
            log.error(error);
            throw new RuntimeException(error);
        }

        chosenGenerator.parse(rootPackage, modelLibrary, fileLibrary, this, modelToParse);
    }
}
