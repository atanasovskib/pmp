package edu.fcse.procesmodelparser.parsers.generators;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import edu.fcse.procesmodelparser.models.ImportDescription;
import edu.fcse.procesmodelparser.models.ArgOrPropertyDescription;
import edu.fcse.procesmodelparser.models.dtos.DtoModel;
import edu.fcse.procesmodelparser.parsers.FileLibrary;
import edu.fcse.procesmodelparser.parsers.GenericParser;
import edu.fcse.procesmodelparser.parsers.ModelLibrary;

import javax.lang.model.element.Modifier;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.function.Function.identity;

public class DtoGenerator extends Generator {
    private final ObjectMapper objectMapper;
    private final String dtoPackageRelative;

    private final Path dtoPath;

    public DtoGenerator(Path rootPath, ObjectMapper objectMapper, String dtoFolder) {
        super(rootPath);
        this.objectMapper = objectMapper;
        this.dtoPackageRelative = dtoFolder;
        this.dtoPath = rootPath.resolve(dtoFolder);
    }

    @Override
    public void parse(String rootPackage,
                      ModelLibrary modelLibrary,
                      FileLibrary fileLibrary,
                      GenericParser genericParser,
                      Path modelPath) {
        DtoModel model = this.readModel(DtoModel.class, this.objectMapper, modelPath);

        Map<String, ImportDescription> importsById = Arrays.stream(model.imports)
                .collect(Collectors.toMap(x -> x.id, identity()));


        if (importsById.size() != model.imports.length) {
            throw new IllegalStateException("Imports in " + modelPath + " contain duplicates");
        }

        Arrays.stream(model.imports)
                .filter(ImportDescription::isLocal)
                .filter(imp -> !modelLibrary.hasDataObject(imp.id))
                .forEach(imp -> {
                    String subFolder = imp.location.substring(1);
                    Path subModelPath = this.rootPath
                            .resolve(subFolder)
                            .resolve(imp.id + ".json");
                    genericParser.parse(rootPackage, modelLibrary, fileLibrary, subModelPath);
                });

        TypeSpec.Builder dtoSpecBuilder = TypeSpec.classBuilder(model.name)
                .addModifiers(Modifier.PUBLIC);

        MethodSpec.Builder constructorBuilder = MethodSpec.constructorBuilder()
                .addModifiers(Modifier.PUBLIC);

        for (ArgOrPropertyDescription property : model.properties) {
            TypeName className = fileLibrary.getTypeName(rootPackage, property.type, importsById);

            dtoSpecBuilder.addField(className, property.name, Modifier.PUBLIC, Modifier.FINAL);
            constructorBuilder.addParameter(className, property.name);
            constructorBuilder.addStatement("this." + property.name + " = " + property.name);
        }


        dtoSpecBuilder.addMethod(constructorBuilder.build());

        String dtosPackage = fileLibrary.packageid(rootPackage, "." + dtoPackageRelative);
        JavaFile dtoJavaFile = JavaFile.builder(dtosPackage, dtoSpecBuilder.build()).build();
        fileLibrary.addFile(model.name, dtoJavaFile);
        modelLibrary.addDataModel(model);
    }

    @Override
    public Path getPath() {
        return this.dtoPath;
    }

}
