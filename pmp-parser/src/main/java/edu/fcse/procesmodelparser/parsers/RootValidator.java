package edu.fcse.procesmodelparser.parsers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class RootValidator {
    private static final Logger log = LoggerFactory.getLogger(RootValidator.class);
    private final String rootPackage;

    private final Path processesPath;
    private final String processesSubfolder;

    private final Path servicesPath;
    private final String servicesSubfolder;

    private final Path dtosPath;
    private final String dtosSubfolder;

    private final Path entitiesPath;
    private final String entitiesSubfolder;

    private final Path rootPath;

    public RootValidator(String rootPackage,
                         Path rootPath,
                         String processesSubfolder,
                         String servicesSubfolder,
                         String dtosSubfolder,
                         String entitiesSubfolder) {
        this.rootPackage = rootPackage;

        this.processesSubfolder = processesSubfolder;
        this.servicesSubfolder = servicesSubfolder;
        this.dtosSubfolder = dtosSubfolder;
        this.entitiesSubfolder = entitiesSubfolder;

        validateRoot(rootPath);

        this.rootPath = rootPath;
        this.processesPath = rootPath.resolve(processesSubfolder);
        this.servicesPath = rootPath.resolve(servicesSubfolder);
        this.dtosPath = rootPath.resolve(dtosSubfolder);
        this.entitiesPath = rootPath.resolve(entitiesSubfolder);

    }


    private void validateRoot(Path root) {
        if (!Files.isDirectory(root)) {
            log.error("Root path [{}] is not a directory", root);
            throw new IllegalArgumentException("Root path is not a directory");
        }

        Set<Path> childrenOfRoot;
        try {
            childrenOfRoot = Files.list(root)
                    .collect(Collectors.toSet());
        } catch (IOException e) {
            log.error("Could not list root folder", e);
            throw new IllegalStateException(e);
        }

        int expectedSubDirectories = 4;
        if (childrenOfRoot.size() != expectedSubDirectories) {
            String error = "Root must have 4 direct sub-folders only processes, services, dtos and entities";
            log.error(error);
            throw new IllegalStateException(error);
        }

        List<String> allowedFirstLevelFolders = Arrays.asList(
                this.processesSubfolder,
                this.servicesSubfolder,
                this.entitiesSubfolder,
                this.dtosSubfolder);

        for (Path firstLevel : childrenOfRoot) {
            boolean isDirectory = Files.isDirectory(firstLevel);
            String folderName = firstLevel.toFile().getName();
            boolean isNameAllowed = allowedFirstLevelFolders.contains(folderName);
            if (!isDirectory || !isNameAllowed) {
                String error = "child of root is not a directory or has invalid name";
                log.error(error);
                throw new IllegalStateException(error);
            }
        }
    }

}
