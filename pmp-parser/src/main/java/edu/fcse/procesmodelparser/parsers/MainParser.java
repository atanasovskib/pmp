package edu.fcse.procesmodelparser.parsers;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.fcse.procesmodelparser.parsers.generators.*;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MainParser {
    public static void main(String[] args) {
        ObjectMapper objectMapper = new ObjectMapper();
        String rootPackage = "edu.fcse.health";
        Path rootPath = Paths.get("./demo_input");

        FileLibrary fileLibrary = new FileLibrary();
        Generator dtoGenerator = new DtoGenerator(rootPath, objectMapper, "dtos");
        Generator entitiesGenerator = new EntitiesGenerator(rootPath, objectMapper, "entities");
        Generator servicesGenerator = new ServiceGenerator(rootPath, objectMapper, "services");
        Generator procesGenerator = new ProcessGenerator(rootPath, objectMapper, "processes");
        Generator userInputGenerator = new UserInputGenerator(rootPath, objectMapper, "userinput");
        GenericParser genericParser = new GenericParser(
                dtoGenerator,
                servicesGenerator,
                entitiesGenerator,
                procesGenerator,
                userInputGenerator);
        ModelLibrary modelLibrary = new ModelLibrary();
        Path processModel = rootPath.resolve("processes").resolve("process.json");
        genericParser.parse(rootPackage, modelLibrary, fileLibrary, processModel);
        System.out.println("Hello");
    }
}
