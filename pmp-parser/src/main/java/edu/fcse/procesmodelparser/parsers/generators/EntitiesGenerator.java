package edu.fcse.procesmodelparser.parsers.generators;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.fcse.procesmodelparser.models.entities.EntitiesModel;
import edu.fcse.procesmodelparser.parsers.FileLibrary;
import edu.fcse.procesmodelparser.parsers.GenericParser;
import edu.fcse.procesmodelparser.parsers.ModelLibrary;

import java.nio.file.Path;
import java.util.Arrays;

public class EntitiesGenerator extends DtoGenerator {
    private final ObjectMapper objectMapper;

    private final Path entitiesPath;

    public EntitiesGenerator(Path rootPath, ObjectMapper objectMapper, String entitiesPackageRelative) {
        super(rootPath, objectMapper, entitiesPackageRelative);
        this.objectMapper = objectMapper;
        this.entitiesPath = rootPath.resolve(entitiesPackageRelative);
    }

    @Override
    public void parse(String rootPackage,
                      ModelLibrary modelLibrary,
                      FileLibrary fileLibrary,
                      GenericParser genericParser,
                      Path modelPath) {
        EntitiesModel model = this.readModel(EntitiesModel.class, this.objectMapper, modelPath);

        boolean hasId = Arrays.stream(model.properties)
                .anyMatch(x -> x.name.equals("id"));

        if (!hasId) {
            throw new RuntimeException("Entities must have an id property! " + model.name + " doesn't have an id");
        }

        super.parse(rootPackage, modelLibrary, fileLibrary, genericParser, modelPath);
        modelLibrary.addDataModel(model);
    }

    @Override
    public Path getPath() {
        return this.entitiesPath;
    }
}
