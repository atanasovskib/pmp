package edu.fcse.procesmodelparser.parsers

import spock.lang.Specification
import spock.lang.Unroll

class TypeLibraryTest extends Specification {
    @Unroll
    def "generatePrimitives=true ->  type for #primitiveTypeName is #expectedPrimitiveType"() {
        given:

        def generatePrimitives = true;
        def typeLibrary = new TypeLibrary(generatePrimitives)

        when:
        def type = typeLibrary.getType(primitiveTypeName)

        then:
        type.isPresent()
        type.get() == expectedPrimitiveType

        where:
        primitiveTypeName | expectedPrimitiveType
        "int"             | int.class
        "int[]"           | int[].class
        "Integer"         | Integer.class
        "Integer[]"       | Integer[].class
        "long"            | long.class
        "long[]"          | long[].class
        "Long"            | Long.class
        "Long[]"          | Long[].class
        "char"            | char.class
        "char[]"          | char[].class
        "Character"       | Character.class
        "Character[]"     | Character[].class
    }

    def "adding new type is later recognized as an array too"() {
        given:
        def generatePrimitives = false
        def library = new TypeLibrary(generatePrimitives)
        library.addType("a", TypeLibraryTest.class)

        when:
        def recognizedType = library.getType(givenTypeName)

        then:
        recognizedType.isPresent()
        recognizedType.get() == expectedType

        where:
        givenTypeName | expectedType
        "a"           | TypeLibraryTest.class
        "a[]"         | TypeLibraryTest[].class
    }

    def "missing type produces empty optional"() {
        given:
        def generatePrimitives = false
        def library = new TypeLibrary(generatePrimitives)

        when:
        def recognizedType = library.getType("unknown type")

        then:
        !recognizedType.isPresent()
    }

    @Unroll
    def "array type #classRegistered can't be added"() {
        given:
        def generatePrimitives = false
        def library = new TypeLibrary(generatePrimitives)

        when:
        library.addType("array", classRegistered)

        then:
        thrown(IllegalArgumentException)

        where:
        classRegistered << [int[].class, String[].class, TypeLibraryTest[].class]
    }
}
