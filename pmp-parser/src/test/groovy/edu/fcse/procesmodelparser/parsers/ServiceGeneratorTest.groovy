package edu.fcse.procesmodelparser.parsers

import com.fasterxml.jackson.databind.ObjectMapper
import edu.fcse.procesmodelparser.models.services.ServiceModel
import edu.fcse.procesmodelparser.parsers.generators.ServiceGenerator
import spock.lang.Specification

import java.nio.file.Files
import java.nio.file.Paths
import java.util.stream.Collectors

class ServiceGeneratorTest extends Specification {

    def kure() {
        given:
        String serviceJson = Files.readAllLines(Paths.get("/home/blagoj/Projects/procesmodelparser/demo_input/services/createReferral2.json"))
                .stream()
                .collect(Collectors.joining())
        ObjectMapper mapper = new ObjectMapper()
        ServiceModel sm = mapper.readValue(serviceJson, ServiceModel.class)
        TypeLibrary typeLibrary = new TypeLibrary(true);
        ServiceGenerator generator = new ServiceGenerator(typeLibrary, new InputParamGenerator());
        generator.generateClass(sm).writeTo(System.out);
    }
}
