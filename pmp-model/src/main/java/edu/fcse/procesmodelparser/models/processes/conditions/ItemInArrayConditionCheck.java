package edu.fcse.procesmodelparser.models.processes.conditions;

import com.fasterxml.jackson.databind.JsonNode;
import edu.fcse.procesmodelparser.models.processes.ProcessState;

public class ItemInArrayConditionCheck implements ConditionCheck<Object> {
    public final int itemNumber;
    public final ConditionCheck<Object[]> array;

    public ItemInArrayConditionCheck(JsonNode node) {
        JsonNode itemNumberNode = node.get("itemNumber");
        this.itemNumber = itemNumberNode.asInt();

        if(this.itemNumber<0){
            throw new IllegalArgumentException("Item number must be >=0");
        }

        JsonNode array = node.get("array");
        String arrayTypeStr = array.get("type").asText();
        CheckType arrayType = CheckType.valueOf(arrayTypeStr);
        this.array = arrayType.parse(array);
    }

    public ItemInArrayConditionCheck(ConditionCheck<Object[]> array, int itemNumber){
        if(itemNumber<0){
            throw new IllegalArgumentException("Item number must be >=0");
        }

        this.itemNumber = itemNumber;
        this.array = array;
    }
    @Override
    public Object check(ProcessState state) {
        Object[] objArray = this.array.check(state);
        return objArray[itemNumber];
    }
}
