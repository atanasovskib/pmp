package edu.fcse.procesmodelparser.models.processes;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import edu.fcse.procesmodelparser.models.processes.conditions.CheckType;
import edu.fcse.procesmodelparser.models.processes.conditions.ConditionCheck;
import edu.fcse.procesmodelparser.models.processes.conditions.IdentityConditionCheck;

public class OutgoingTransition {
    public final String from;
    public final String to;
    public final ConditionCheck condition;

    @JsonCreator
    public OutgoingTransition(
            @JsonProperty("from") String from,
            @JsonProperty("to") String to,
            @JsonProperty("condition") JsonNode condition) {
        this.from = from;
        this.to = to;
        if (condition == null || condition.isNull()) {
            this.condition = new IdentityConditionCheck();
            return;
        }

        String type = condition.get("type").asText();
        CheckType checkType = CheckType.valueOf(type);
        this.condition = checkType.parse(condition);
    }
}
