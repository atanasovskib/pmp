package edu.fcse.procesmodelparser.models.entities;

public abstract class DataObject {
    public final String name;

    protected DataObject(String name) {
        this.name = name;
    }
}
