package edu.fcse.procesmodelparser.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class ArgOrPropertyDescription {
    public final String name;
    public final String type;

    @JsonCreator
    public ArgOrPropertyDescription(
            @JsonProperty("name") String name,
            @JsonProperty("type") String type) {
        this.name = Objects.requireNonNull(name);
        this.type = Objects.requireNonNull(type);
    }
}
