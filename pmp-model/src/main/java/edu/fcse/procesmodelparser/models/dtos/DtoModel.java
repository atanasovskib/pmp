package edu.fcse.procesmodelparser.models.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import edu.fcse.procesmodelparser.models.ImportDescription;
import edu.fcse.procesmodelparser.models.ArgOrPropertyDescription;
import edu.fcse.procesmodelparser.models.entities.DataObject;

public class DtoModel extends DataObject {
    public final ImportDescription[] imports;
    public final ArgOrPropertyDescription[] properties;

    @JsonCreator
    public DtoModel(
            @JsonProperty("imports") ImportDescription[] imports,
            @JsonProperty("name") String name,
            @JsonProperty("properties") ArgOrPropertyDescription[] properties) {
        super(name);
        this.imports = imports;
        this.properties = properties;
    }
}
