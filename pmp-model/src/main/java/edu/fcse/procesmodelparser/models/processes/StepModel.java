package edu.fcse.procesmodelparser.models.processes;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class StepModel {
    public final StepType stepType;
    public final String id;
    public final String location;

    @JsonCreator
    public StepModel(
            @JsonProperty("stepType") StepType stepType,
            @JsonProperty("id") String id,
            @JsonProperty("location") String location) {
        this.stepType = Objects.requireNonNull(stepType);
        this.id = Objects.requireNonNull(id);
        this.location = Objects.requireNonNull(location);
    }
}
