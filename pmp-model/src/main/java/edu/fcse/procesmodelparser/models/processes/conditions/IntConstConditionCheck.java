package edu.fcse.procesmodelparser.models.processes.conditions;

import com.fasterxml.jackson.databind.JsonNode;
import edu.fcse.procesmodelparser.models.processes.ProcessState;

import java.util.Objects;

public class IntConstConditionCheck implements ConditionCheck<Integer> {
    public final Integer value;

    public IntConstConditionCheck(JsonNode node) {
        this.value = node.asInt();
    }

    public IntConstConditionCheck(Integer value) {
        this.value = Objects.requireNonNull(value);
    }

    @Override
    public Integer check(ProcessState state) {
        return value;
    }
}
