package edu.fcse.procesmodelparser.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ImportDescription {
    public final String id;
    public final String location;
    public final boolean isLocal;

    @JsonCreator
    public ImportDescription(@JsonProperty("id") String id,
                             @JsonProperty("location") String location) {
        this.id = id;
        this.location = location;
        this.isLocal = location.startsWith(".");
    }

    public boolean isLocal() {
        return this.isLocal;
    }
}
