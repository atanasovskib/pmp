package edu.fcse.procesmodelparser.models.processes.conditions;

import com.fasterxml.jackson.databind.JsonNode;
import edu.fcse.procesmodelparser.models.processes.ProcessState;

import java.util.Arrays;
import java.util.function.Predicate;

public class AnyMatchConditionCheck implements ConditionCheck<Boolean> {
    public final ConditionCheck<Object[]> array;
    public final CheckType comparator;
    public final ConditionCheck<Object> compareTo;
    private final BinaryChecker binaryChecker;

    public AnyMatchConditionCheck(JsonNode node) {
        JsonNode arrayNode = node.get("array");
        CheckType arrayType = CheckType.valueOf(arrayNode.get("type").asText());
        this.array = arrayType.parse(arrayNode);

        JsonNode comparatorNode = node.get("comparator");
        this.comparator = CheckType.valueOf(comparatorNode.asText());

        if (!this.comparator.returnsBool) {
            throw new IllegalArgumentException("Comparator does not return bool");
        }

        JsonNode compareToNode = node.get("compareTo");
        CheckType compareToType = CheckType.valueOf(compareToNode.get("type").asText());
        this.compareTo = compareToType.parse(node);

        this.binaryChecker = new BinaryChecker();
    }

    public AnyMatchConditionCheck(ConditionCheck<Object[]> array, CheckType comparator,
                                  ConditionCheck<Object> compareTo, BinaryChecker binaryChecker) {
        if (!comparator.returnsBool) {
            throw new IllegalArgumentException("Comparator must return bool");
        }

        this.array = array;
        this.comparator = comparator;
        this.compareTo = compareTo;
        this.binaryChecker = binaryChecker;
    }

    @Override
    public Boolean check(ProcessState state) {
        Object[] arrayObj = array.check(state);
        Object compareValue = this.compareTo.check(state);
        Predicate<Object> itemComparator = item -> binaryChecker.check(comparator, item, compareValue);
        return Arrays.stream(arrayObj)
                .anyMatch(itemComparator);
    }
}
