package edu.fcse.procesmodelparser.models.services;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ConstraintChecks {
    public final CheckType type;
    public final String property;
    public final String value;

    @JsonCreator
    public ConstraintChecks(
            @JsonProperty("type") CheckType type,
            @JsonProperty("property") String property,
            @JsonProperty("value") String value) {
        this.type = type;
        this.property = property;
        this.value = value;
    }
}
