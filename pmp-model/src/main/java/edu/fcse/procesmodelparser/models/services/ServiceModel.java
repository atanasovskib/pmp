package edu.fcse.procesmodelparser.models.services;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import edu.fcse.procesmodelparser.models.ArgOrPropertyDescription;
import edu.fcse.procesmodelparser.models.ImportDescription;
import edu.fcse.procesmodelparser.models.StepImplementation;
import edu.fcse.procesmodelparser.models.processes.StepType;

import java.util.Arrays;

public class ServiceModel implements StepImplementation {
    public final String name;
    public final ImportDescription[] imports;
    public final ArgOrPropertyDescription[] input;
    public final String output;
    public final SideEffectDescription sideEffects;

    @JsonCreator
    public ServiceModel(@JsonProperty("name") String name,
                        @JsonProperty("imports") ImportDescription[] imports,
                        @JsonProperty("input") ArgOrPropertyDescription[] input,
                        @JsonProperty("output") String output,
                        @JsonProperty("sideEffects") SideEffectDescription sideEffects) {
        this.name = name;
        this.imports = imports;
        this.input = input;
        this.output = output;
        this.sideEffects = sideEffects;
    }

    @Override
    public String toString() {
        return "ServiceModel{" +
                "name='" + name + '\'' +
                ", imports=" + Arrays.toString(imports) +
                ", input=" + Arrays.toString(input) +
                ", output='" + output + '\'' +
                ", sideEffects=" + sideEffects +
                '}';
    }

    @Override
    public ArgOrPropertyDescription[] getInput() {
        return this.input;
    }

    @Override
    public String getOutput() {
        return this.output;
    }

    @Override
    public String getStepId() {
        return this.name;
    }

    @Override
    public StepType getStepType() {
        return StepType.SERVICE;
    }
}
