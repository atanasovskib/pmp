package edu.fcse.procesmodelparser.models.processes;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class OutputReference {
    public final String stepId;
    public final String propertyName;

    @JsonCreator
    public OutputReference(
            @JsonProperty("stepId") String stepId,
            @JsonProperty("propertyName") String propertyName) {
        this.stepId = Objects.requireNonNull(stepId);
        this.propertyName = Objects.requireNonNull(propertyName);
    }
}
