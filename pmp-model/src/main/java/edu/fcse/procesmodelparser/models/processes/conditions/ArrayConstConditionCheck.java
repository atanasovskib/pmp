package edu.fcse.procesmodelparser.models.processes.conditions;

import com.fasterxml.jackson.databind.JsonNode;
import edu.fcse.procesmodelparser.models.processes.ProcessState;

import java.util.Objects;

public class ArrayConstConditionCheck implements ConditionCheck<Object[]> {
    public final Object[] array;

    public ArrayConstConditionCheck(JsonNode node) {
        String subTypeStr = node.get("subType").asText();
        CheckType subType = CheckType.valueOf(subTypeStr);

        JsonNode arrayNode = node.get("array");
        if (!arrayNode.isArray()) {
            throw new IllegalArgumentException("Array const can not be created from a node that doesn't have an array property");
        }

        array = new Object[arrayNode.size()];
        for (int i = 0; i < array.length; i++) {
            JsonNode currentElement = arrayNode.get(i);
            array[i] = readConst(subType, currentElement);
        }
    }

    public <T> ArrayConstConditionCheck(T[] value) {
        this.array = Objects.requireNonNull(value);
    }

    private Object readConst(CheckType subType, JsonNode value) {
        switch (subType) {
            case STRING_CONST:
                return value.asText();
            case INT_CONST:
                return value.asInt();
        }

        throw new IllegalArgumentException("Item in array not of specified sub type");
    }

    @Override
    public Object[] check(ProcessState state) {
        return array;
    }
}
