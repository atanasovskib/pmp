package edu.fcse.procesmodelparser.models.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import edu.fcse.procesmodelparser.models.ImportDescription;
import edu.fcse.procesmodelparser.models.ArgOrPropertyDescription;

public class EntitiesModel extends DataObject{
    public final ImportDescription[] imports;
    public final ArgOrPropertyDescription[] properties;

    @JsonCreator
    public EntitiesModel(
            @JsonProperty("imports") ImportDescription[] imports,
            @JsonProperty("name") String name,
            @JsonProperty("properties") ArgOrPropertyDescription[] properties) {
        super(name);
        this.imports = imports;
        this.properties = properties;
    }
}
