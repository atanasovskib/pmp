package edu.fcse.procesmodelparser.models.processes.conditions;

import com.fasterxml.jackson.databind.JsonNode;
import edu.fcse.procesmodelparser.models.processes.ProcessState;

import java.lang.reflect.Field;
import java.util.Objects;

public class GetFromStateConditionCheck implements ConditionCheck<Object> {
    public final String stepId;
    private final String propertyName;

    public GetFromStateConditionCheck(JsonNode node) {
        JsonNode stepIdNode = node.get("stepId");
        this.stepId = stepIdNode.asText();

        if (node.has("propertyName")) {
            JsonNode propertyName = node.get("propertyName");
            this.propertyName = propertyName.asText();
        } else {
            this.propertyName = null;
        }
    }

    public GetFromStateConditionCheck(String stepId, String propertyName) {
        this.stepId = Objects.requireNonNull(stepId);
        this.propertyName = Objects.requireNonNull(propertyName);
    }

    public GetFromStateConditionCheck(String stepId) {
        this.stepId = stepId;
        this.propertyName = null;
    }

    @Override
    public Object check(ProcessState state) {
        Object completeReturn = state.getStateForStep(this.stepId);
        if (this.propertyName == null) {
            return completeReturn;
        }
        Class<?> classOfReturnedResult = completeReturn.getClass();
        try {
            Field fieldOfReturnedResult = classOfReturnedResult.getField(this.propertyName);
            return fieldOfReturnedResult.get(completeReturn);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            String s = "Returned object of class " + classOfReturnedResult +
                    " doesn't have an accessible field named " + this.propertyName;
            throw new IllegalStateException(s);
        }
    }
}
