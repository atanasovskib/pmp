package edu.fcse.procesmodelparser.models.processes.conditions;

import com.fasterxml.jackson.databind.JsonNode;
import edu.fcse.procesmodelparser.models.processes.ProcessState;

import java.util.Objects;

public class StringConstConditionCheck implements ConditionCheck<String> {
    public final String value;

    public StringConstConditionCheck(JsonNode node) {
        this.value = node.asText();
    }

    public StringConstConditionCheck(String value) {
        this.value = Objects.requireNonNull(value);
    }

    @Override
    public String check(ProcessState state) {
        return value;
    }
}
