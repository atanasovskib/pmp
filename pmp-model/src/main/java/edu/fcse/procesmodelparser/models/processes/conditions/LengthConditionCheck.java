package edu.fcse.procesmodelparser.models.processes.conditions;

import com.fasterxml.jackson.databind.JsonNode;
import edu.fcse.procesmodelparser.models.processes.ProcessState;

public class LengthConditionCheck implements ConditionCheck<Integer> {
    public final ConditionCheck<Object[]> array;

    public LengthConditionCheck(JsonNode node) {
        String typeOfNodeStr = node.get("type").asText();
        CheckType typeOfNode = CheckType.valueOf(typeOfNodeStr);
        if (typeOfNode != CheckType.LENGTH) {
            throw new IllegalArgumentException("Type property of JsonNode must be " + CheckType.LENGTH);
        }

        JsonNode array = node.get("array");
        String arrayTypeStr = array.get("type").asText();

        CheckType arrayType = CheckType.valueOf(arrayTypeStr);
        this.array = arrayType.parse(array);
    }

    public LengthConditionCheck(ConditionCheck<Object[]> array) {
        this.array = array;
    }

    @Override
    public Integer check(ProcessState state) {
        Object[] objArray = array.check(state);
        return objArray.length;
    }
}