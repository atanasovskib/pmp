package edu.fcse.procesmodelparser.models.processes.conditions;

import com.fasterxml.jackson.databind.JsonNode;
import edu.fcse.procesmodelparser.models.processes.ProcessState;

public class BinaryConditionCheck implements ConditionCheck<Boolean> {
    public final ConditionCheck left;
    public final ConditionCheck right;
    public final CheckType checkType;
    public final BinaryChecker conditionChecker;

    public BinaryConditionCheck(JsonNode node) {
        JsonNode left = node.get("left");
        JsonNode right = node.get("right");
        String leftTypeStr = left.get("type").asText();
        String rightTypeStr = right.get("type").asText();

        CheckType leftType = CheckType.valueOf(leftTypeStr);
        CheckType rightType = CheckType.valueOf(rightTypeStr);
        this.left = leftType.parse(left);
        this.right = rightType.parse(right);

        this.checkType = CheckType.valueOf(node.get("type").asText());
        this.conditionChecker = new BinaryChecker();
    }

    public BinaryConditionCheck(ConditionCheck left, ConditionCheck right,
                                CheckType checkType, BinaryChecker binaryChecker) {
        this.left = left;
        this.right = right;
        this.checkType = checkType;
        this.conditionChecker = binaryChecker;
    }

    @Override
    public Boolean check(ProcessState state) {
        Object leftValue = left.check(state);
        Object rightValue = right.check(state);
        return conditionChecker.check(this.checkType, leftValue, rightValue);
    }

}
