package edu.fcse.procesmodelparser.models.processes.conditions;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.function.Function;

public enum CheckType {
    GET_FROM_STATE(GetFromStateConditionCheck::new, false),
    AND(BinaryConditionCheck::new, true),
    OR(BinaryConditionCheck::new, true),
    XOR(BinaryConditionCheck::new, true),
    LENGTH(LengthConditionCheck::new, false),
    ITEM_IN_ARRAY(ItemInArrayConditionCheck::new, false),
    EQ(BinaryConditionCheck::new, true),
    NEQ(BinaryConditionCheck::new, true),
    LT(BinaryConditionCheck::new, true),
    LTE(BinaryConditionCheck::new, true),
    GT(BinaryConditionCheck::new, true),
    GTE(BinaryConditionCheck::new, true),
    ANY_MATCH(AnyMatchConditionCheck::new, true),
    STRING_CONST(StringConstConditionCheck::new, false),
    INT_CONST(IntConstConditionCheck::new, false),
    ARRAY_CONST(ArrayConstConditionCheck::new, false),
    ALL_MATCH(AllMatchConditionCheck::new, true);

    public final boolean returnsBool;
    private Function<JsonNode, ConditionCheck> parser;

    CheckType(Function<JsonNode, ConditionCheck> parser, boolean returnsBool) {
        this.parser = parser;
        this.returnsBool = returnsBool;
    }

    public <T> ConditionCheck<T> parse(JsonNode checkJson) {
        return parser.apply(checkJson);
    }
}
