package edu.fcse.procesmodelparser.models.processes;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.google.common.collect.Tables;

import java.util.*;

public class ProcessModel {
    public final String processId;
    public final String startingTask;
    public final String endingTask;
    public final List<StepModel> steps;
    public final Table<String, String, List<ArgumentMapping>> argumentMappings;
    public final Map<String, List<OutgoingTransition>> transitions;

    @JsonCreator
    public ProcessModel(
            @JsonProperty("processId") String processId,
            @JsonProperty("startingTask") String startingTask,
            @JsonProperty("endingTask") String endingTask,
            @JsonProperty("steps") StepModel[] steps,
            @JsonProperty("argumentMappings") ArgumentMapping[] argumentMappings,
            @JsonProperty("transitions") OutgoingTransition[] transitions
    ) {
        this.processId = processId;
        this.startingTask = startingTask;
        this.endingTask = endingTask;
        this.steps = Collections.unmodifiableList(Arrays.asList(steps));
        Table<String, String, List<ArgumentMapping>> mutableArgumentMappings = HashBasedTable.create(
                steps.length, steps.length);
        for (ArgumentMapping am : argumentMappings) {
            if (!mutableArgumentMappings.contains(am.from, am.to)) {
                mutableArgumentMappings.put(am.from, am.to, new LinkedList<>());
            }

            mutableArgumentMappings.get(am.from, am.to).add(am);
        }

        this.argumentMappings = Tables.unmodifiableTable(mutableArgumentMappings);
        Map<String, List<OutgoingTransition>> mutableTransitions = new HashMap<>();
        for (OutgoingTransition transition : transitions) {
            List<OutgoingTransition> transitionsFromStep = mutableTransitions.computeIfAbsent(
                    transition.from,
                    (src) -> new LinkedList<>());
            transitionsFromStep.add(transition);
        }

        this.transitions = Collections.unmodifiableMap(mutableTransitions);
    }
}
