package edu.fcse.procesmodelparser.models.services;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SideEffectDescription {
    public final ConstraintDescription[] created;
    public final ConstraintDescription[] updated;
    public final ConstraintDescription[] deleted;

    @JsonCreator
    public SideEffectDescription(
            @JsonProperty("created") ConstraintDescription[] created,
            @JsonProperty("updated") ConstraintDescription[] updated,
            @JsonProperty("deleted") ConstraintDescription[] deleted) {
        this.created = created;
        this.updated = updated;
        this.deleted = deleted;
    }
}
