package edu.fcse.procesmodelparser.models;

import edu.fcse.procesmodelparser.models.processes.StepType;

public interface StepImplementation {
    ArgOrPropertyDescription[] getInput();

    String getOutput();

    String getStepId();

    StepType getStepType();
}
