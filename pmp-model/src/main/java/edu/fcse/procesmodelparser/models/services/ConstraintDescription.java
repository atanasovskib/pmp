package edu.fcse.procesmodelparser.models.services;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ConstraintDescription {
    public final String id;
    public final String entityType;
    public final ConstraintChecks[] constraints;

    @JsonCreator
    public ConstraintDescription(
            @JsonProperty("id") String id,
            @JsonProperty("entityType") String entityType,
            @JsonProperty("constraints") ConstraintChecks[] constraints) {
        this.id = id;
        this.entityType = entityType;
        this.constraints = constraints;
    }
}
