package edu.fcse.procesmodelparser.models.processes.conditions;

public class BinaryChecker {
    public boolean check(CheckType checkType, Object leftValue, Object rightValue) {
        switch (checkType) {
            case AND:
                boolean leftBool = (boolean) leftValue;
                boolean rightBool = (boolean) rightValue;
                return leftBool && rightBool;
            case OR:
                leftBool = (boolean) leftValue;
                rightBool = (boolean) rightValue;
                return leftBool || rightBool;
            case XOR:
                leftBool = (boolean) leftValue;
                rightBool = (boolean) rightValue;
                return leftBool ^ rightBool;
            case EQ:
                return leftValue.equals(rightValue);
            case NEQ:
                return !leftValue.equals(rightValue);
            case LT:
                Comparable leftComp = (Comparable) leftValue;
                Comparable rightComp = (Comparable) rightValue;
                return leftComp.compareTo(rightComp) < 0;
            case LTE:
                leftComp = (Comparable) leftValue;
                rightComp = (Comparable) rightValue;
                return leftComp.compareTo(rightComp) <= 0;
            case GT:
                leftComp = (Comparable) leftValue;
                rightComp = (Comparable) rightValue;
                return leftComp.compareTo(rightComp) > 0;
            case GTE:
                leftComp = (Comparable) leftValue;
                rightComp = (Comparable) rightValue;
                return leftComp.compareTo(rightComp) >= 0;
        }

        throw new IllegalStateException("Binary condition with " + checkType + " as comparator");
    }
}
