package edu.fcse.procesmodelparser.models.processes.conditions;

import com.fasterxml.jackson.databind.JsonNode;
import edu.fcse.procesmodelparser.models.processes.ProcessState;

import java.util.Arrays;
import java.util.function.Predicate;

public class AllMatchConditionCheck implements ConditionCheck<Boolean> {
    public final ConditionCheck<? extends Object[]> array;
    public final CheckType comparator;
    public final ConditionCheck<Object> compareTo;
    public final BinaryChecker binaryChecker;

    protected AllMatchConditionCheck(JsonNode node) {

        JsonNode arrayNode = node.get("array");
        CheckType arrayType = CheckType.valueOf(arrayNode.get("type").asText());
        this.array = arrayType.parse(arrayNode);

        JsonNode comparatorNode = node.get("comparator");
        this.comparator = CheckType.valueOf(comparatorNode.asText());

        if(!this.comparator.returnsBool){
            throw new IllegalArgumentException("Comparator does not return bool");
        }

        JsonNode compareToNode = node.get("compareTo");
        CheckType compareToType = CheckType.valueOf(compareToNode.get("type").asText());
        this.compareTo = compareToType.parse(node);
        this.binaryChecker = new BinaryChecker();
    }

    public AllMatchConditionCheck(ConditionCheck<Object[]> array, CheckType comparator,
                                  ConditionCheck<Object> compareTo, BinaryChecker checker) {
        if(!comparator.returnsBool){
            throw new IllegalArgumentException("Comparator does not return bool");
        }

        this.array = array;
        this.comparator = comparator;
        this.compareTo = compareTo;
        this.binaryChecker = checker;
    }

    @Override
    public Boolean check(ProcessState state) {
        Object[] arrayObj = array.check(state);
        Object compareValue = this.compareTo.check(state);
        Predicate<Object> itemComparator = item -> binaryChecker.check(comparator, item, compareValue);
        return Arrays.stream(arrayObj)
                .allMatch(itemComparator);
    }
}
