package edu.fcse.procesmodelparser.models.processes;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.function.Function;

public class ArgumentMapping {
    public final String from;
    public final String to;
    public final String inputParameterName;
    public final OutputReference outputReference;

    @JsonCreator
    public ArgumentMapping(
            @JsonProperty("from") String from,
            @JsonProperty("to") String to,
            @JsonProperty("inputParameterName") String inputParameterName,
            @JsonProperty("outputReference") OutputReference outputReference) {
        this.from = from;
        this.to = to;
        this.inputParameterName = inputParameterName;
        this.outputReference = outputReference;
    }

}
