package edu.fcse.procesmodelparser.models.services;

public enum CheckType {
    EQUALS, NOT_EQUALS, GT, LT, GTE, LTE
}
