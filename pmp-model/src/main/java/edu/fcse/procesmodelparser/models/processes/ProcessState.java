package edu.fcse.procesmodelparser.models.processes;

import java.util.HashMap;
import java.util.Map;

public class ProcessState {
    private Map<String, Object> stateByStep;

    public ProcessState() {
        this.stateByStep = new HashMap<>();
    }

    public ProcessState(Map<String, Object> stateByStep) {
        this.stateByStep = new HashMap<>();
        this.stateByStep.putAll(stateByStep);
    }

    public Object getStateForStep(String stepId) {
        if (!this.stateByStep.containsKey(stepId)) {
            throw new RuntimeException("no such step in state: " + stepId);
        }

        return this.stateByStep.get(stepId);
    }
}
