package edu.fcse.procesmodelparser.models.processes.conditions;

import edu.fcse.procesmodelparser.models.processes.ProcessState;

public interface ConditionCheck<T> {
    T check(ProcessState state);
}
