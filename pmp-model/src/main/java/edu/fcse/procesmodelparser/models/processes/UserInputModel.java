package edu.fcse.procesmodelparser.models.processes;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import edu.fcse.procesmodelparser.models.ArgOrPropertyDescription;
import edu.fcse.procesmodelparser.models.ImportDescription;
import edu.fcse.procesmodelparser.models.StepImplementation;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class UserInputModel implements StepImplementation {
    public final ImportDescription[] imports;
    private final ArgOrPropertyDescription[] input;
    private final String output;
    private final String name;

    @JsonCreator
    public UserInputModel(
            @JsonProperty("imports") ImportDescription[] imports,
            @JsonProperty("input") ArgOrPropertyDescription[] input,
            @JsonProperty("output") String output,
            @JsonProperty("name") String name) {
        this.name = Objects.requireNonNull(name);

        this.imports = Objects.requireNonNull(imports);
        this.input = Objects.requireNonNull(input);

        if (!noDuplicateImports(imports)) {
            throw new IllegalArgumentException("imports of " + name + " UserInputModel contain duplicate types");
        }

        if (!eachInputIsImported(input, imports)) {
            throw new IllegalArgumentException("not all input types are imported in the " + name + " are imported");
        }

        this.output = Objects.requireNonNull(output);
        if (!outputIsNotImported(output, imports)) {
            throw new IllegalArgumentException("Output type of " + name + " UserInputModel is not imported");
        }

    }

    private boolean noDuplicateImports(ImportDescription[] imports) {
        long numDistinct = Arrays.stream(imports)
                .map(x -> x.id)
                .distinct()
                .count();

        return numDistinct == imports.length;
    }

    private boolean eachInputIsImported(ArgOrPropertyDescription[] input, ImportDescription[] imports) {
        Set<String> importNames = Arrays.stream(imports)
                .map(x -> x.id)
                .collect(Collectors.toSet());
        return Arrays.stream(input)
                .map(x -> x.type)
                .allMatch(importNames::contains);
    }

    private boolean outputIsNotImported(String output, ImportDescription[] imports) {
        Set<String> importNames = Arrays.stream(imports)
                .map(x -> x.id)
                .collect(Collectors.toSet());
        return importNames.contains(output);
    }

    @Override
    public ArgOrPropertyDescription[] getInput() {
        return input;
    }

    @Override
    public String getOutput() {
        return output;
    }

    @Override
    public String getStepId() {
        return name;
    }

    @Override
    public StepType getStepType() {
        return StepType.USER_INPUT;
    }
}
