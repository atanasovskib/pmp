package edu.fcse.procesmodelparser.models.processes;

public enum StepType {
    PROCESS, SERVICE, USER_INPUT
}
