package edu.fcse.procesmodelparser.models.processes;

import edu.fcse.procesmodelparser.models.ArgOrPropertyDescription;
import edu.fcse.procesmodelparser.models.StepImplementation;

public class ParsedProcessModel implements StepImplementation {

    public final ProcessModel processModel;
    private final StepImplementation startingStep;
    private final StepImplementation endingStep;

    public ParsedProcessModel(
            ProcessModel processModel,
            StepImplementation startingStep,
            StepImplementation endingStep) {
        this.processModel = processModel;
        this.startingStep = startingStep;
        this.endingStep = endingStep;
    }

    @Override
    public ArgOrPropertyDescription[] getInput() {
        return startingStep.getInput();
    }

    @Override
    public String getOutput() {
        return endingStep.getOutput();
    }

    @Override
    public String getStepId() {
        return processModel.processId;
    }

    @Override
    public StepType getStepType() {
        return StepType.PROCESS;
    }
}
