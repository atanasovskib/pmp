package edu.fcse.procesmodelparser.models.processes

import com.fasterxml.jackson.databind.ObjectMapper
import edu.fcse.procesmodelparser.models.ArgOrPropertyDescription
import edu.fcse.procesmodelparser.models.ImportDescription
import spock.lang.Specification

class UserInputModelTest extends Specification {
    def "can deserializa"() {
        given:
        def om = new ObjectMapper()
        def fileLoc = "UserInputModelExample.json"
        def resource = getClass().getClassLoader().getResource(fileLoc)
        def model = resource.text

        when:
        def deserialized = om.readValue(model, UserInputModel)

        then:
        deserialized.input.length == 1
        deserialized.output == "Integer"
        deserialized.stepId == "Dummy"
    }

    def "no duplicate imports allowed"() {
        given:
        def name = "Dummy"
        def output = "Integer"
        def input = [] as ArgOrPropertyDescription[]
        def imports = [new ImportDescription("Integer", "java.lang"),
                       new ImportDescription("Integer", "java.lang")] as ImportDescription[]

        when:
        new UserInputModel(imports, input, output, name)

        then:
        thrown(IllegalArgumentException)
    }

    def "each input param must be imported, if not fail"() {
        given:
        def name = "Dummy"
        def output = "Integer"
        def faultyImports = [new ImportDescription("Integer", "java.lang")] as ImportDescription[]
        def okImports = [new ImportDescription("Long", "java.long"),
                         new ImportDescription("Integer", "java.long")] as ImportDescription[]
        def input = [new ArgOrPropertyDescription("arg", "Long")] as ArgOrPropertyDescription[]

        when:
        new UserInputModel(faultyImports, input, output, name)

        then:
        thrown(IllegalArgumentException)
    }

    def "output must be imported"() {
        given:
        def name = "Dummy"
        def output = "Integer"
        def faultyImports = [new ImportDescription("Long", "java.long")] as ImportDescription[]
        def input = [new ArgOrPropertyDescription("arg", "Long")] as ArgOrPropertyDescription[]

        when:
        new UserInputModel(faultyImports, input, output, name)

        then:
        thrown(IllegalArgumentException)
    }
}
