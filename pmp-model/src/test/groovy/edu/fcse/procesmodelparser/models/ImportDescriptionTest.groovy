package edu.fcse.procesmodelparser.models

import com.fasterxml.jackson.databind.ObjectMapper
import spock.lang.Specification
import spock.lang.Unroll

class ImportDescriptionTest extends Specification {
    def "can derialize without any config"() {
        given:
        ObjectMapper om = new ObjectMapper()
        def resource = getClass().getClassLoader().getResource("ImportDescriptionLocalExample.json")

        def jsonString = resource.text

        when:
        ImportDescription desc = om.readValue(jsonString, ImportDescription)

        then:
        desc != null
    }

    @Unroll
    def "can distinguish local/non local"() {
        given:
        def om = new ObjectMapper()

        when:
        def fileContent = getClass().getClassLoader().getResource(fileLoc).text
        and:
        def importDescription = om.readValue(fileContent, ImportDescription)

        then:
        importDescription.isLocal == isLocal

        where:
        fileLoc                                 | isLocal
        "ImportDescriptionLocalExample.json"    | true
        "ImportDescriptionNonLocalExample.json" | false
    }
}
