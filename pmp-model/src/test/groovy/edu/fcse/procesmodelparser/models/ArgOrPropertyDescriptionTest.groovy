package edu.fcse.procesmodelparser.models

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.exc.InvalidDefinitionException
import spock.lang.Specification

class ArgOrPropertyDescriptionTest extends Specification {
    def "can deserialize without any config"() {
        given:
        def om = new ObjectMapper()
        def fileLoc = "PropertyDescriptionExample.json"
        def resource = getClass().getClassLoader().getResource(fileLoc)
        def model = resource.text

        when:
        def result = om.readValue(model, ArgOrPropertyDescription)

        then:
        result.name != null
        result.type != null
    }

    def "can not deserialize if some properties are null"() {
        given:
        def om = new ObjectMapper()
        def fileLoc = "PropertyDescriptionWrongExample.json"
        def resource = getClass().getClassLoader().getResource(fileLoc)
        def model = resource.text

        when:
        om.readValue(model, ArgOrPropertyDescription)

        then:
        thrown(InvalidDefinitionException.class)
    }
}
