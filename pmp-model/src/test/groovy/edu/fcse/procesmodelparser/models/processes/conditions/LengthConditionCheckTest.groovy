package edu.fcse.procesmodelparser.models.processes.conditions

import com.fasterxml.jackson.databind.ObjectMapper
import edu.fcse.procesmodelparser.models.processes.ProcessState
import spock.lang.Specification

class LengthConditionCheckTest extends Specification {
    def "when given a condition check that returns an array returns length of array"() {
        given:
        def state = Mock(ProcessState)

        def arrayToReturn = [1, 2, 3] as Integer[]

        def mockCheck = Mock(ConditionCheck)
        mockCheck.check(_ as ProcessState) >> arrayToReturn
        def lengthConditionCheck = new LengthConditionCheck(mockCheck)

        when:
        def length = lengthConditionCheck.check(state)

        then:
        arrayToReturn.length == length
    }

    def "when given a condition check that doesn't return an array it fails"() {
        given:
        def state = Mock(ProcessState)
        def toReturn = 1 as Integer
        def mockCheck = Mock(ConditionCheck)
        mockCheck.check(_ as ProcessState) >> toReturn
        def lengthConditionCheck = new LengthConditionCheck(mockCheck)

        when:
        lengthConditionCheck.check(state)

        then:
        thrown(ClassCastException)
    }

    def "can deserialize from json"() {
        def om = new ObjectMapper()
        def fileLoc = "LengthConditionCheckExample.json"
        def resource = getClass().getClassLoader().getResource(fileLoc)
        def model = resource.text
        def state = Mock(ProcessState)
        def jsonNode = om.readTree(model)

        when:
        def parsed = new LengthConditionCheck(jsonNode)
        
        then:
        parsed.array
        parsed.check(state) == 3
    }
}
