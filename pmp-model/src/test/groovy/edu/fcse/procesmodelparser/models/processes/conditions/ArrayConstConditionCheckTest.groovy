package edu.fcse.procesmodelparser.models.processes.conditions

import com.fasterxml.jackson.databind.ObjectMapper
import edu.fcse.procesmodelparser.models.processes.ProcessState
import spock.lang.Specification

class ArrayConstConditionCheckTest extends Specification {
    def "can deserialize from json with supported sub type"() {
        def om = new ObjectMapper()
        def fileLoc = "ArrayConst_withInts_ConditionCheckExample.json"
        def resource = getClass().getClassLoader().getResource(fileLoc)
        def model = resource.text
        def state = Mock(ProcessState)
        def jsonNode = om.readTree(model)

        when:
        def parsed = new ArrayConstConditionCheck(jsonNode)

        then:
        parsed.array
        parsed.check(state) == [1, 2, 3] as Integer[]
    }

    def "can not deserialize from json with unsupported sub type"() {
        def om = new ObjectMapper()
        def fileLoc = "ArrayConst_withDates_ConditionCheckExample.json"
        def resource = getClass().getClassLoader().getResource(fileLoc)
        def model = resource.text
        def jsonNode = om.readTree(model)

        when:
        new ArrayConstConditionCheck(jsonNode)

        then:
        thrown(IllegalArgumentException)
    }

    def "Can deserialize array length 0"() {
        def om = new ObjectMapper()
        def fileLoc = "ArrayConst_emptyArray_ConditionCheckExample.json"
        def resource = getClass().getClassLoader().getResource(fileLoc)
        def model = resource.text
        def state = Mock(ProcessState)
        def jsonNode = om.readTree(model)
        int[] emptyArray = new int[0]
        when:
        def parsed = new ArrayConstConditionCheck(jsonNode)

        then:
        parsed.array.length == 0
        parsed.check(state) == emptyArray
    }

    def "can't deserialize from json with array not an array"() {
        def om = new ObjectMapper()
        def fileLoc = "ArrayConst_NotAnArray_ConditionCheckExample.json"
        def resource = getClass().getClassLoader().getResource(fileLoc)
        def model = resource.text
        def jsonNode = om.readTree(model)

        when:
        new ArrayConstConditionCheck(jsonNode)

        then:
        thrown(IllegalArgumentException)
    }

    def "Wrong subtype results in zeroes at array item values"() {
        def om = new ObjectMapper()
        def fileLoc = "ArrayConst_WrongSubtype_ConditionCheckExample.json"
        def resource = getClass().getClassLoader().getResource(fileLoc)
        def model = resource.text
        def jsonNode = om.readTree(model)
        def check = new ArrayConstConditionCheck(jsonNode)
        def state = Mock(ProcessState)

        when:
        def returned = check.check(state)
        then:
        returned.length == 2
        returned[0] == 1
        returned[1] == 0
    }
}
