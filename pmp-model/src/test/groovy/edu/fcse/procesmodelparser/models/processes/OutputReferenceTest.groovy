package edu.fcse.procesmodelparser.models.processes

import com.fasterxml.jackson.databind.ObjectMapper
import spock.lang.Specification

class OutputReferenceTest extends Specification {
    def "can deserialize without additional config"() {
        given:
        def om = new ObjectMapper()
        def fileLoc = "OutputReferenceExample.json"
        def resource = getClass().getClassLoader().getResource(fileLoc)
        def model = resource.text

        when:
        def parsed = om.readValue(model, OutputReference)

        then:
        parsed.propertyName
        parsed.stepId
    }
}
